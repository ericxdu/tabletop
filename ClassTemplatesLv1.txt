Feats and Skills in [Brackets] are for bonuses

Str 13, Dex 8, Con 14, Int 10, Wis 15, Cha 12 (Wis+5)
Cleric 1d8, Fort +2, Ref +0, Will +2
Feat: Scribe Scroll, [Alertness]
Special: Turn/Rebuke Undead, Divine Spellcasting(Clr)
Armor: Scale Mail, Heavy Wooden Shield
Weapons: Heavy Mace, Light Crossbow
Skills: Spellcraft, Concentration, [Heal, Knowledge(religion), Diplomacy]

Str 15, Dex 13, Con 14, Int 10, Wis 12, Cha 8 (Str+5)
Fighter 1d10, Fort +2, Ref +0, Will +0
Feat: Weapon Focus, [Blind-Fight]
Special: Power Attack
Armor: Scale Mail, Heavy Wooden Shield
Weapons: Greatsword, Shortbow
Skills: Climb, Jump, [Ride, Swim, Intimidate]

Str 12, Dex 15, Con 13, Int 14, Wis 10, Cha 8 (Dex+5)
Rogue 1d6, Fort +0, Ref +2, Will +0
Feat: Improved Initiative, [Alertness]
Special: Sneak Attack, Trapfinding
Armor: Leather
Weapons: Short Sword, Light Crossbow, Dagger
Skills: Move Silently, Hide, Spot, Listen, Search, Disable Device, Open Lock, Climb, [Use Magic Device, Sleight of Hand, Decipher Script, Bluff, Intimidate]

Str 10, Dex 14, Con 13, Int 15, Wis 12, Cha 8 (Int+5)
Wizard 1d4, Fort +0, Ref +0, Will +2
Feat: Toughness, [Combat Casting]
Special: Scribe Scroll, Summon Familiar, Arcane Spellcasting(Wiz)
Armor: None
Weapons: Quarterstaff, Light Crossbow
Skills: Spellcraft, Concentration, [Knowledge(arcana), Decipher Script]
