Most of these play aids are based on the 3rd edition of Dungeons & Dragons, released with the Open Game License. See [D20 SRD](http://www.opengamingfoundation.org/srd.html) for details.

Art for the icon is from [lukems-br](https://opengameart.org/content/rpg-ui-buttonsicons)