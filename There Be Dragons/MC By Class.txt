Warrior: HP 10, Fort +2, Refl +1, Will +0
Scale Mail (Total Armor 14)
Greatsword	2d6+3	19/x2
Shortbow	1d6	19/x2
Power Attack 1 (+1 to attack or melee damage)

Healer: HP 8, Fort +1, Refl +0, Will +2
Scale Mail and Heavy Shield (Total Armor 16)
Heavy Mace 	1d8+1	20/x2
Light Crossbow	1d8	19/x2
Cure Light Wounds 1d8+1

Assassin: HP 6, Fort +1, Refl +2, Will +0
Leather Armor (Total Armor 12)
Rapier		1d6	18/x2
Shortbow	1d6	20/x2
Sneak Attack 1 (+1d6 damage to flank)

Magician: HP 6, Fort +0, Refl +1, Will +2
No Armor (Total Armor 10)
Quarterstaff	1d6	20/x2
Light Crossbow	1d8	19/x2
Magic Missile 1d4+1)
