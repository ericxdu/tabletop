Round: all combatants get a Turn, in order

Turn: each combatant can Move once and Act once

Move: any number of orthogonal spaces up to Move score

Act: any miscellaneous action, including Attack

Attack: target adjacent monster, roll 1d20

    if the result is equal to or higher than the Difficulty Class
    to hit the monster, the monster is Wounded.

Wound: reduce the monster's Hit Points by 1

Various character traits and equipment modify the above rules.
