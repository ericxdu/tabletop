Large Zombie 2/2 Move 6
HD 2d12 Fort +1 Ref +1 Will +3
Armor 12 (+9 large +3 natural)
Slam +5, 1d8+5
Single Actions Only (Ex)

Huge Zombie 3/3 Move 6
HD 4d12 Fort +1 Ref +1 Will +4
Armor 11 (+8 size +4 natural -1 dex)
Slam +9, 2d6+9
Single Actions Only (Ex)
