Medium Vermin 1/1 Move 6
HD 2d8 Fort +0 Ref +0 Will +3
Armor 12 (+2 natural)
Bite +, 1d6

Large Vermin 2/2 Move 6
HD 4d8
Armor 14 (+5 natural -1 size)
Bite +, 1d8+4

Huge Vermin 3/3 Move 6
HD 8d8
Armor 18 (+10 natural -2 size)
Bite +, 2d6+8
