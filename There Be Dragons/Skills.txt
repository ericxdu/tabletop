Save Bonus Feats: Great Fortitude, Iron Will, Lightning Reflexes

Sorted by DnD5e category
Strength
	Athletics: climb, jump, swim
Constitution
	Endurance: concentrate
Dexterity
	Acrobatics: tumble, balance, ride
	Sleight of Hand: craft, disable device, escape artist, open lock,
                     pick pocket, use rope
	Stealth: hide, move silently
Intelligence
	Arcana: appraise, decipher script, spellcraft
	History:
	Investigation: gather information, search
	Nature:
	Religion:
Wisdom
	Animals: handle animal
	Insight: sense motive
	Medicine: heal
	Perception: listen, spot
	Survival: wilderness lore
Charisma
	Deception: disguise, bluff, forgery
	Intimidation: intimidate
	Performance: perform, use magic device
	Persuasion: diplomacy


Sorted by Feat (all 36 DnD3.5 Skills)
Acrobatic: Jump, Tumble
Agile: Balance, Escape Artist
Alertness: Listen, Spot
Animal Affinity: Handle Animal, Ride
Athletic: Climb, Swim
Deceptive: Disguise, Forgery
Deft Hands: Sleight of Hand, Use Rope
Diligent: Appraise, Decipher Script
Investigator: Gather Information, Search
Magical Aptitude: Spellcraft, Use Magic Device
Negotiator: Diplomacy, Sense Motive
Nimble Fingers: Disable Device, Open Lock
Persuasive: Bluff, Intimidate
Self-Sufficient: Heal, Survival
Stealthy: Hide, Move Silently


Concentrate
Craft
Knowledge
Speak Language
Performance
Profession


Synergies (rank 5+ -> check +2)
Bluff -> Diplomacy, Disguise, Intimidate, Sleight of Hand
Craft -> Appraise
Decipher Script -> Use Scroll
Escape Artist -> Use Rope
Handle Animal -> Ride, wild empathy
*** Jump -> Tumble (Acrobatic)
Knowledge (arcana) -> Spellcraft
Knowledge (engineering) -> Search (secret doors)
Knowledge (dungeoneering) -> Survival (underground)
Knowledge (geography) -> Survival (natural hazards)
Knowledge (history) -> Bardic Knowledge
Knowledge (local) -> Gather Information
Knowledge (nature) -> Survival (aboveground natural)
Knowledge (nobility and royalty) -> Diplomacy
Knowledge (religion) -> turn undead
Knowledge (the planes) -> Survival (foreign planes)
Search -> Survival (tracking)
Sense Motive -> Diplomacy
*** Spellcraft -> Use Scroll (Magical Aptitude)
Survival -> Knowledge (nature)
Tumble -> Balance, Jump
*** Use Magic Device -> Spellcraft (scrolls) (Magical Aptitude)
Use Rope -> Climb, Escape Artist


