Movement	(can move one space)
Bonuses:
	Strength	(+1 to damage roll)
	Accuracy	(+1 to attack roll)
	Luck		(-1 threat range)
	Evasion		(+1 to Armor Class)
	Speed		(1 extra attack)

Attack:
	Speed		(# of attacks)
	Accuracy	(bonus to attack)
	Strength	(bonus to damage)
	
Progression:
	Experience (Age)
	Proficiency (Specialization, Mastery)
	Courage (Boldness)
	Understanding (Insight)

Survival	(spend to avoid damage)
Toughness (monster): difficulty to damage



Courage
Understanding
Permanent Luck
Permanent Accuracy
Temporary Survival
Experience -> Age
Movement

Survival (Expendable, Maximum)
Armor == HP (Body Points, Wounds)
Blind Spot == Flanked
Toughness (monster)
Strength (attack)
Speed (attack)
Insanity
+1 damage token
+1 understanding	(Reason?)
+1 insanity		(Willpower?)
+1 courage		(Confidence?)


Flanked: flankers gain +2 attack against target
Critical Threat:
Bull Rush: random enemy action
Charge: random enemy action
