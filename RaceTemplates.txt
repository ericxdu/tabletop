Human
Speed: 30ft/20ft
Bonus Feat
Bonus Skill

Dwarf (+2 Con, -2 Cha)
Speed: 20ft/20ft
Darkvision 60ft
Stonecunning 10ft, Search +2 (racial), trap, depth
Weapon Familiarity: Dwarven Waraxe, Dwarven Ugrosh
Stability +4 (bull rush, trip)
Saves: poison +2 (racial) spells/spell-like +2 (racial)
attack orcs/goblinoids +1 (racial)
AC: giant type +4 (dodge)
Appraise(stone/metal) +2 (racial)
Craft(stone/metal) +2 (racial)

Halfling (+2 Dex, -2 Str)
Speed: 20ft/15ft
AC +1 (size)
attack +1 (size)
thrown weapon +1 (racial, stacking)
sling +1 (racial, stacking)
Saves: Fort +1 Ref +1 Will +1 (racial) fear +1 (racial, stacking)
Hide +4 (size)
Climb +2 (racial)
Jump +2 (racial)
Move Silently +2 (racial)
Listen +2 (racial)

Elf (+2 Dex, -2 Con)
Speed: 30ft/20ft
Immune to sleep magic effects
Saves: enchantment +2 (racial)
Low-light Vision
Weapon Proficiency: longsword, rapier, longbow, shortbow
Doorfinding 5ft
Listen +2 (racial)
Search +2 (racial)
Spot +2 (racial)
