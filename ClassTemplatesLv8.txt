Feats and Skills in [Brackets] are for bonuses

Cleric 8d8, Fort +6, Ref +2, Will +6
Str 13, Dex 8, Con 14, Int 10, Wis 17, Cha 12
Feat: Scribe Scroll, Brew Potion, Lightning Reflexes, [Alertness]
Special: Turn Undead, Divine Spellcasting
Armor: Scale Mail, Heavy Wooden Shield
Weapons: Heavy Mace, Light Crossbow
Skills: Spellcraft, Concentration, [Heal, Knowledge(religion), Diplomacy]

Fighter 8d10, Fort +6, Ref +2, Will +2
Str 17, Dex 13, Con 14, Int 10, Wis 12, Cha 8
Feat: Weapon Focus, Weapon Specialization, Dodge, [Blind-Fight]
Special Feat: Power Attack, Improved Initiative, Cleave, Great Cleave, Improved Critical
Armor: Scale Mail, Heavy Wooden Shield
Weapons: Greatsword, Shortbow
Skills: Climb, Jump, [Ride, Swim, Intimidate]

Rogue 8d6, Fort +2, Ref +6, Will +2
Str 12, Dex 17, Con 13, Int 14, Wis 10, Cha 8
Feat: Improved Initiative, Shield Proficiency, Lightning Reflexes [Alertness]
Special: Sneak Attack, Trapfinding, Evasion, Uncanny Dodge
Armor: Leather
Weapons: Short Sword, Light Crossbow, Dagger
Skills: Move Silently, Hide, Spot, Listen, Search, Disable Device, Open Lock, Climb, [Use Magic Device, Sleight of Hand, Decipher Script, Bluff, Intimidate]

Wizard 8d4, Fort +2, Ref +2, Will +6
Str 10, Dex 14, Con 13, Int 17, Wis 12, Cha 8
Feat: Toughness, Brew Potion, Great Fortitude, [Combat Casting]
Special Feat: Spell Penetration
Special: Summon Familiar, Scribe Scroll, Arcane Spellcasting
Armor: None
Weapons: Quarterstaff, Light Crossbow
Skills: Spellcraft, Concentration, [Knowledge(arcana), Decipher Script]
